//
//  CMOverlayView.m
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import "CMOverlayView.h"
#import <PureLayout.h>

@interface CMOverlayView()
@property (strong, nonatomic) NSLayoutConstraint *labelYPositionConstraint;
@end

@implementation CMOverlayView

+ (CMOverlayView *)shared{
	static dispatch_once_t onceToken;
	static CMOverlayView *shared;
	
	dispatch_once(&onceToken, ^{
		shared = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds];
	});
	
	return shared;
}

- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	
	if (self){
		self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.93];
		UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		[self addSubview:messageLabel];
		messageLabel.text = @"Buscando...";
		self.labelYPositionConstraint = [messageLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
		[messageLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
	}
	
	return self;
}

+ (void)show{
	[[self shared] addToViewHierachy];
	[self shared].alpha = 0;
	[self shared].labelYPositionConstraint.constant = -500;
	[[self shared] layoutIfNeeded];
	[UIView animateWithDuration:.4
					 animations:^{
						 [self shared].alpha = 1;
						 [self shared].labelYPositionConstraint.constant = 0;
						 [[self shared] layoutIfNeeded];
					 }
					 completion:nil];
}

+ (void)hide{
	[UIView animateWithDuration:.4
					 animations:^{
						 [self shared].alpha = 0;
						 [self shared].labelYPositionConstraint.constant = 500;
						 [[self shared] layoutIfNeeded];
					 }
					 completion:nil];
}

- (void)addToViewHierachy {
	
	if(!self.superview) {
		UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
		[keyWindow addSubview:self];
	} else {
		[self.superview bringSubviewToFront:self];
	}
	
}
@end
