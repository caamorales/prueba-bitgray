//
//  CMOverlayView.h
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMOverlayView : UIView
+ (void)show;
+ (void)hide;
@end
