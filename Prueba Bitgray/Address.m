//
//  Address.m
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import "Address.h"
#import <CoreLocation/CLLocation.h>

@implementation Address

+ (Address *)addressFrom:(NSDictionary *)data{
	
	Address *anAddress = [Address new];
	
	anAddress.street = data[@"street"];
	anAddress.suite = data[@"suite"];
	anAddress.city = data[@"city"];
	anAddress.zipCode = data[@"zipcode"];
	
	NSDictionary *geo = data[@"geo"];
	
	anAddress.location = [[CLLocation alloc] initWithLatitude:[geo[@"lat"] doubleValue] longitude:[geo[@"lng"] doubleValue] ];
	
	return anAddress;
	
}

@end
