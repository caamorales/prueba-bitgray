//
//  ViewController.m
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import "ViewController.h"
#import "User.h"
#import "Address.h"
#import "Company.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CMOverlayView.h"

@interface ViewController () <CLLocationManagerDelegate>
@property (strong, nonatomic) NSArray <User *> *users;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *closestUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *furtherUserLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (weak, nonatomic) IBOutlet UILabel *locationNotAuthorizedLabel;
@end

@implementation ViewController

#pragma mark Setters and Getters

- (CLLocationManager *)locationManager{
	if (!_locationManager){
		_locationManager = [[CLLocationManager alloc] init];
		_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		_locationManager.delegate = self;
	}
	
	return _locationManager;
}

- (void)setUsers:(NSArray<User *> *)users{
	[self getUserLocation];
	_users = users;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self loadUsers];
}

#pragma mark IBActions

- (IBAction)refreshButtonTapped:(UIBarButtonItem *)sender {
	
	[CMOverlayView show];
	
	//Simulate delay to show animation
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		[CMOverlayView hide];
	});
	
	if (!self.users){
		[self loadUsers];
		return;
	}
	
	[self addRandomUserToMap];
	[self getUserLocation];
}

#pragma mark Location 

- (void)getUserLocation{
	CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
	if (![CLLocationManager locationServicesEnabled] || status == kCLAuthorizationStatusDenied) {
		self.locationNotAuthorizedLabel.hidden = NO;
		return;
	}
	if (status == kCLAuthorizationStatusNotDetermined) {
		[self.locationManager requestWhenInUseAuthorization];
	} else {
		[self.locationManager startUpdatingLocation];
	}
}

#pragma mark Helpers

- (void)loadUsers{
	[User getUsers:^(NSArray<User *> *users, NSError *error) {
		if (!error){
			self.users = users;
			[self addRandomUserToMap];
		}else{
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mensaje"
																		   message:@"Ocurrió un error al descargar la lista de usuarios, por favor intenta de nuevo." preferredStyle:UIAlertControllerStyleAlert];
			[alert addAction:[UIAlertAction actionWithTitle:@"Aceptar"
													  style:UIAlertActionStyleDefault
													handler:nil]];
			[self presentViewController:alert animated:YES completion:nil];
		}
	}];
}

- (void)addRandomUserToMap{
	
	//Clear previous user
	
	[self.mapView clear];
	
	//Get Random User
	
	NSUInteger randomUserIndex = arc4random() % self.users.count;
	
	User *randomUser = self.users[randomUserIndex];
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:randomUser.address.location.coordinate.latitude
															longitude:randomUser.address.location.coordinate.longitude
																 zoom:4];

	[self.mapView animateToCameraPosition:camera];
	GMSMarker *marker = [GMSMarker new];
	marker.position = randomUser.address.location.coordinate;
	marker.title = randomUser.name;
	marker.snippet = randomUser.username;
	marker.map = self.mapView;
	
}

- (void)updateClosestAndFurtherUsers{
	
	if (!self.currentLocation){
		return;
	}
	
	User *closestUser = [self.users firstObject];
	User *furtherUser = [self.users firstObject];
	
	for (User *anUser in self.users) {
		
		//Check for closest user
		
		if ([anUser.address.location distanceFromLocation:self.currentLocation] < [closestUser.address.location distanceFromLocation:self.currentLocation]){
			closestUser = anUser;
		}
		
		//Check for further user
		
		if ([anUser.address.location distanceFromLocation:self.currentLocation] > [furtherUser.address.location distanceFromLocation:self.currentLocation]){
			furtherUser = anUser;
		}
	}
	
	self.furtherUserLabel.text = [NSString stringWithFormat:@"%@\n%@\n%.2f km",furtherUser.name,furtherUser.username, [furtherUser.address.location distanceFromLocation:self.currentLocation] / 1000];
	
	self.closestUserLabel.text = [NSString stringWithFormat:@"%@\n%@\n%.2f km",closestUser.name,closestUser.username, [closestUser.address.location distanceFromLocation:self.currentLocation] / 1000];
	
}

 #pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{

}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray*)locations{
	CLLocation *location = [locations lastObject];
	self.currentLocation = location;
	self.locationNotAuthorizedLabel.hidden = YES;
	[self updateClosestAndFurtherUsers];
	[self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
	if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
		[self.locationManager startUpdatingLocation];
		self.locationNotAuthorizedLabel.hidden = YES;
	}else{
		self.locationNotAuthorizedLabel.hidden = NO;
	}
}



@end
