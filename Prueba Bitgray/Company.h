//
//  Company.h
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Company : NSObject

+ (Company *)companyFrom:(NSDictionary *) data;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *catchPhrase;
@property (strong, nonatomic) NSString *bs;
@end
