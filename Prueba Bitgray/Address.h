//
//  Address.h
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface Address : NSObject

+ (Address *)addressFrom:(NSDictionary *) data;

@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *suite;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) CLLocation *location;
@end
