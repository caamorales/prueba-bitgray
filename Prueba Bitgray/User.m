//
//  User.m
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import "User.h"
#import "Address.h"
#import "Company.h"
#import <AFNetworking.h>

@implementation User

+ (User *)userFrom:(NSDictionary *) data{
	
	User *anUser = [User new];
	
	anUser.userID = data[@"id"];
	anUser.name = data[@"name"];
	anUser.username = data[@"username"];
	anUser.email = data[@"email"];
	anUser.phone = data[@"phone"];
	anUser.website = data[@"website"];
	anUser.address = [Address addressFrom:data[@"address"]];
	anUser.company = [Company companyFrom:data[@"company"]];
	
	return anUser;
	
}

+ (NSArray <User *> *)usersFrom:(NSArray <NSDictionary *> *) usersData{
	
	if (![usersData isKindOfClass:[NSArray class]]){
		return @[];
	}
	
	NSMutableArray <User *> *users = [[NSMutableArray alloc] initWithCapacity:usersData.count];
	
	for (NSDictionary *anUser in usersData) {
		[users addObject:[self userFrom:anUser]];
	}
	
	return users;
}

+ (void)getUsers:(getUsersCompletion) completion{
	
	//Setup Session Manager
	
	AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
	sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
	sessionManager.requestSerializer.timeoutInterval = 5.0;
	
	//Get Users
	
	[sessionManager GET:@"http://jsonplaceholder.typicode.com/users"
			 parameters:nil
			   progress:nil
				success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
					completion([self usersFrom:responseObject], nil);
				} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
					completion(nil, [NSError errorWithDomain:@"Network Error" code:-1 userInfo:nil]);
				}];
	
}


@end
