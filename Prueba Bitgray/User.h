//
//  User.h
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;
@class Address;
@class Company;

typedef void(^getUsersCompletion)(NSArray <User *> *users, NSError *error);

@interface User : NSObject

+ (void)getUsers:(getUsersCompletion) completion;

@property (strong, nonatomic) NSNumber *userID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *website;
@property (strong, nonatomic) Address *address;
@property (strong, nonatomic) Company *company;

@end
