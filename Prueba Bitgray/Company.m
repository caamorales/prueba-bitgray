//
//  Company.m
//  Prueba Bitgray
//
//  Created by Camilo Morales Pérez on 6/9/17.
//  Copyright © 2017 Camilo Morales. All rights reserved.
//

#import "Company.h"

@implementation Company

+ (Company *)companyFrom:(NSDictionary *)data{
	
	Company *aCompany = [Company new];
	
	aCompany.name = data[@"name"];
	aCompany.catchPhrase = data[@"catchPhrase"];
	aCompany.bs = data[@"bs"];
	
	return aCompany;
}

@end
